const express = require('express');
const nunjucks = require('nunjucks');
const path = require('path');
const bodyParser = require('body-parser');
const moment = require('moment');


moment.locale('pt-BR');
const app = express();


nunjucks.configure('views', {
  autoescape: true,
  express: app,
});

app.set('view engine', 'njk');

app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (req, res) => {
  res.render('main');
});

app.get('/major', (req, res) => {
  res.render('major', { nome: req.query.nome, idade: req.query.idade });
});

app.get('/minor', (req, res) => {
  res.render('minor', { nome: req.query.nome, idade: req.query.idade });
});

app.post('/check', (req, res) => {
  const { nome, data_nascimento } = req.body;
  const date = moment(data_nascimento).format('L');
  const idade = moment().diff(moment(date, 'DD/MM/YYYY'), 'years');
  if (idade > 18) {
    res.redirect(`/major?nome=${nome}&idade=${idade}`);
  } else {
    res.redirect(`/minor?nome=${nome}&idade=${idade}`);
  }
});



app.listen(3000);
